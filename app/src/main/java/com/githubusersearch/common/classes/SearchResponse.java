package com.githubusersearch.common.classes;


import java.util.List;
/**
 * totalCount : 12
 * incompleteResults : false
 * items : [{"login":"mojombo","id":1,"avatarUrl":"https://secure.gravatar.com/avatar/25c7c18223fb42a4c6ae1c8db6f50f9b?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png","gravatarId":"","url":"https://api.github.com/users/mojombo","htmlUrl":"https://github.com/mojombo","followersUrl":"https://api.github.com/users/mojombo/followers","subscriptionsUrl":"https://api.github.com/users/mojombo/subscriptions","organizationsUrl":"https://api.github.com/users/mojombo/orgs","reposUrl":"https://api.github.com/users/mojombo/repos","receivedEventsUrl":"https://api.github.com/users/mojombo/received_events","type":"User","score":105.47857}]
 */

public class SearchResponse {

    private int totalCount;
    private boolean incompleteResults;
    private List<ItemsBean> items;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }
}
