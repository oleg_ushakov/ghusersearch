package com.githubusersearch.common.classes;

public class APIError {
    private int statusCode;
    private String message;

    public APIError() {

    }

    public APIError(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }
    public String getMessage() {
        return message;
    }
}