package com.githubusersearch.common.interfaces;

import com.githubusersearch.common.classes.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubServiceClass {
    @GET("search/users")
    Call<SearchResponse> requestUserList(@Query("q") String queryText);
}
