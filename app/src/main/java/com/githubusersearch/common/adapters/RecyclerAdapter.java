package com.githubusersearch.common.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.githubusersearch.R;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter <RecyclerAdapter.ViewHolder> {
    private List<String> dataset;

    public RecyclerAdapter(List<String> dataset) {
        this.dataset = dataset;
    }

    /**
     * class for receiving each element in list item
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        private ViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.tv_recycler_item);
        }
    }

    /**
     * create new views
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        // тут можно программно менять layouts attributes (size, margins, paddings и др.)
        return new ViewHolder(v);
    }

    // set content for each view item
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
